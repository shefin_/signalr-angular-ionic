import { Component } from '@angular/core';
import { SignalrService } from '../services/signalr.service';
import { NgZone } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  public userNamePending: string;
  public message: string;
  public userName: string;
  public messages: Array<MessageData>;

  public sendMessage() {
    let data: MessageData = new MessageData(this.userName, this.message);
    this.signalRService.broadcastMessage(data);
    
    // Clear the message box.
    this.message = '';
  }

  public submitUsername() {
    this.userName = this.userNamePending;
  }

  private addMessageToChatBox(name: string, message: string) {
    let messageData: MessageData = new MessageData(name, message);
    this.messages.push(messageData);
  }

  private handleMessageFromServer(name: string, message: string) {
    // This will ensure that the Angular change detection mechanism is aware of the changes made by SignalR and updates the UI accordingly.
    this.ngZone.run(() => this.addMessageToChatBox(name, message));
  }

  constructor(private signalRService: SignalrService, private ngZone: NgZone) {
    this.userNamePending = '';
    this.message = '';
    this.userName = '';
    this.messages = new Array<MessageData>();

    this.signalRService.initializeSignalRConnection();
  }

  ngOnInit() {
    this.signalRService.proxy.on('addMessageToChatBox', $.proxy(this.handleMessageFromServer, this));
  }
}

class MessageData {
  public name: string;
  public message: string;

  constructor(_name: string = '', _message: string = '') {
    this.name = _name;
    this.message = _message;
  }
}
