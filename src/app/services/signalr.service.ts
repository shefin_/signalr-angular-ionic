import { Injectable } from '@angular/core';

declare var $: any;

interface MessageData {
  name: string,
  message: string;
}

@Injectable({
  providedIn: 'root'
})
export class SignalrService {
  private connection: any;
  public proxy: any;

  constructor() { }

  public initializeSignalRConnection(): void {
    let signalRServerEndPoint = 'http://localhost:44394/signalr';
    this.connection = $.hubConnection(signalRServerEndPoint);
    this.proxy = this.connection.createHubProxy('chatHub'); // The name of the signalr hub on the server

    this.connection.start().done((data: any) => {
      console.log('Connected to Chat Hub');
    }).catch((error: any) => {
      console.log('Chat Hub error -> ' + error);
    });
  }

  public broadcastMessage(data: MessageData): void {
    this.proxy.invoke('sendMessage', data.name, data.message)
      .catch((error: any) => {
        console.log('broadcastMessage error -> ' + error);
      });
  }
}

